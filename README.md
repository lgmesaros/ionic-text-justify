# Text Justification in Ionic

Simple app that transforms left-align text to justify text and uses hyphenation to control the spacing widths.

This is based on TeX - line breaking algorithm in JavaScript written by Knuth and Plass.

https://github.com/bramstein/typeset/