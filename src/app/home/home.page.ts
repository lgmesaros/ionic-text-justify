import { Component, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { HypherService } from '../typeset/hypher.service';
import { LineBreakService } from '../typeset/line-break.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [HypherService, LineBreakService]
})
export class HomePage {
  @ViewChild('typeset') typesetDiv: ElementRef;
  @ViewChild('section') sectionDiv: ElementRef;
  lineLength: number = 0;
  lineHeight: number = 0;
  lineLengths: Number[] = [];
  tmp: any[];
  ruler: any;
  h: HypherService;
  lb: LineBreakService;
  cache: any = {};
  space: {
    width: number,
    stretch: number,
    shrink: number
  };
  hyphenWidth: number;
  hyphenPenalty: number = 100;
  justified: boolean = false;

  constructor(private el: ElementRef, private renderer: Renderer2) {
    this.el.nativeElement.ownerDocument.body.style.fontSize = '100%';
    this.el.nativeElement.ownerDocument.body.style.lineHeight = '20px';
    this.el.nativeElement.ownerDocument.body.style.wordWrap = 'normal';
    this.el.nativeElement.ownerDocument.body.style.backgroundColor = 'rgb(180, 180, 180)';
    this.el.nativeElement.ownerDocument.body.style.padding = '0';
    this.el.nativeElement.ownerDocument.body.style.margin = '0';
  }

  ionViewWillEnter() {
    // Save the left-align text (default) to local storage
    window.localStorage.setItem('chapter', this.sectionDiv.nativeElement.innerHTML);

    this.h = new HypherService();
    this.lb = new LineBreakService();

    this.setRuler();

    // Calculate the space widths based on our font preferences
    this.ruler.innerHTML = '&nbsp;';
    //console.log(this.ruler.offsetWidth);
    this.space = {
      width: this.ruler.offsetWidth,
      stretch: (this.ruler.offsetWidth * 3) / 6,
      shrink: (this.ruler.offsetWidth * 3) / 9
    };
    //console.log(this.space);
    this.hyphenWidth = this.measureString('-');

    this.ruler.innerHTML = 'Hello World';
    this.lineHeight = parseFloat(this.ruler.style.lineHeight.replace('px', ''));
    //console.log(this.lineHeight);
  }

  justifyText() {
    this.lineLength = this.sectionDiv.nativeElement.offsetWidth;
    //console.log(this.lineLength);
    
    Array.prototype.slice.call(this.sectionDiv.nativeElement.children).forEach(element => {
      let nodes = [],
        breaks = [],
        output = [],
        lines = [],
        words,
        carryOver = null,
        i, point, r, lineStart;

      if (element.nodeName.toUpperCase() === 'P') {
        words = element.textContent.split(/\s/);
        //console.log(words);
        words.forEach((word, index, array) => {
          var hyphenated = [];
          if (word.length > 6) {
            hyphenated = this.h.hyphenate(word);
          }

          if (hyphenated.length > 1) {
            hyphenated.forEach((part, partIndex, partArray) => {
              nodes.push(this.lb.box(this.measureString(part), part));
              if (partIndex !== partArray.length - 1) {
                nodes.push(this.lb.penalty(this.hyphenWidth, this.hyphenPenalty, 1));
              }
            });
          } else {
            nodes.push(this.lb.box(this.measureString(word), word));
          }

          if (index === array.length - 1) {
            nodes.push(this.lb.glue(0, this.lb.infinity, 0));
            nodes.push(this.lb.penalty(0, -this.lb.infinity, 1));
          } else {
            nodes.push(this.lb.glue(this.space.width, this.space.stretch, this.space.shrink));
          }
        });

        //console.log(nodes);

        // Perform the line breaking
        breaks = this.lb.linebreak(nodes, this.lineLengths.length !== 0 ? this.lineLengths : [this.lineLength], { tolerance: 1 });

        // Try again with a higher tolerance if the line breaking failed.
        if (breaks.length === 0) {
          breaks = this.lb.linebreak(nodes, this.lineLengths.length !== 0 ? this.lineLengths : [this.lineLength], { tolerance: 2 });
          // And again
          if (breaks.length === 0) {
            breaks = this.lb.linebreak(nodes, this.lineLengths.length !== 0 ? this.lineLengths : [this.lineLength], { tolerance: 3 });
          }
        }

        // Build lines from the line breaks found.
        for (i = 1; i < breaks.length; i += 1) {
          point = breaks[i].position,
            r = breaks[i].ratio;

          for (var j = lineStart; j < nodes.length; j += 1) {
            // After a line break, we skip any nodes unless they are boxes or forced breaks.
            if (nodes[j].type === 'box' || (nodes[j].type === 'penalty' && nodes[j].penalty === -this.lb.infinity)) {
              lineStart = j;
              break;
            }
          }
          lines.push({ ratio: r, nodes: nodes.slice(lineStart, point + 1), position: point });
          lineStart = point;
        }

        //console.log(lines);

        lines.forEach((line, lineIndex, lineArray) => {
          let indent = false,
            spaces = 0,
            totalAdjustment = 0,
            wordSpace = line.ratio * (line.ratio < 0 ? this.space.shrink : this.space.stretch),
            integerWordSpace = Math.round(wordSpace),
            adjustment = wordSpace - integerWordSpace,
            integerAdjustment = adjustment < 0 ? Math.floor(adjustment) : Math.ceil(adjustment),
            tmp = [];

          // Iterate over the nodes in each line and build a temporary array containing just words, spaces, and soft-hyphens.
          line.nodes.forEach((n, index, array) => {
            // normal boxes
            if (n.type === 'box' && n.value !== '') {
              if (tmp.length !== 0 && tmp[tmp.length - 1] !== '&nbsp;') {
                tmp[tmp.length - 1] += n.value;
              } else {
                tmp.push(n.value);
              }
              // empty boxes (indentation for example)
            } else if (n.type === 'box' && n.value === '') {
              output.push('<span style="margin-left: 30px;"></span>');
              // glue inside a line
            } else if (n.type === 'glue' && index !== array.length - 1) {
              tmp.push('&nbsp;');
              spaces += 1;
              // glue at the end of a line
            } else if (n.type === 'glue') {
              tmp.push(' ');
              // hyphenated word at the end of a line
            } else if (n.type === 'penalty' && n.penalty === this.hyphenPenalty && index === array.length - 1) {
              tmp.push('&shy;');
              // Remove trailing space at the end of a paragraph
            } else if (n.type === 'penalty' && index === array.length - 1 && tmp[tmp.length - 1] === '&nbsp;') {
              tmp.pop();
            }
          });

          totalAdjustment = Math.round(adjustment * spaces);

          // If the line ends at a soft hyphen we need to do something special as Webkit doesn't properly handle <span>hy&shy;</span><span>phen</span>.
          if (tmp[tmp.length - 1] === '&shy;') {
            if (totalAdjustment !== 0) {
              output.push('<span style="word-spacing: ' + (integerWordSpace + integerAdjustment) + 'px;">' + (carryOver ? carryOver : '') + tmp.slice(0, Math.abs(totalAdjustment) * 2).join('') + '</span>');
              output.push('<span style="word-spacing: ' + integerWordSpace + 'px;">' + tmp.slice((Math.abs(totalAdjustment) * 2), -2).join('') + '</span>');
            } else {
              output.push('<span style="word-spacing: ' + integerWordSpace + 'px;">' + (carryOver ? carryOver : '') + tmp.slice(0, -2).join('') + "</span>");
            }
            carryOver = tmp.slice(-2).join('');
          } else {
            if (totalAdjustment !== 0) {
              output.push('<span style="word-spacing: ' + (integerWordSpace + integerAdjustment) + 'px;">' + (carryOver ? carryOver : '') + tmp.slice(0, Math.abs(totalAdjustment) * 2).join('') + '</span>');
              output.push('<span style="word-spacing: ' + integerWordSpace + 'px;">' + tmp.slice(Math.abs(totalAdjustment) * 2).join('') + '</span>');
            } else {
              output.push('<span style="word-spacing: ' + integerWordSpace + 'px;">' + (carryOver ? carryOver : '') + tmp.join('') + "</span>");
            }
            carryOver = null;
          }
        });

        //console.log(element.innerHTML);
        element.innerHTML = output.join('');
        this.lineLengths = this.lineLengths.slice(lines.length);
      }
    });
  }

  setRuler(): any {
    this.ruler = this.renderer.createElement('div');
    this.renderer.addClass(this.ruler, 'ruler');
    //this.ruler.innerHTML = '&nbsp;';
    //this.renderer.setStyle(this.ruler, 'visibility', 'hidden');
    //this.renderer.setStyle(this.ruler, 'position', 'absolute');
    this.renderer.setStyle(this.ruler, 'top', '-8000px');
    this.renderer.setStyle(this.ruler, 'width', 'auto');
    this.renderer.setStyle(this.ruler, 'display', 'inline');
    this.renderer.setStyle(this.ruler, 'left', '-8000px');
    this.renderer.setStyle(this.ruler, 'lineHeight', '20px');
    document.body.appendChild(this.ruler);
    //this.renderer.appendChild(this.el.nativeElement, this.ruler);
  }

  measureString(str: string) {
    if (!this.cache[str]) {
      this.ruler.firstChild.nodeValue = str;
      this.cache[str] = this.ruler.offsetWidth;
    }
    return this.cache[str];
  }

  alignText() {
    this.justified = !this.justified;
    if(this.justified === true) {
      this.justifyText();
      this.justified = true;
    }
    else {
      this.sectionDiv.nativeElement.innerHTML = window.localStorage.getItem('chapter');
    }
  }
}
