import { Injectable } from '@angular/core';
import { LinkedListService } from './linked-list.service';

@Injectable()
export class LineBreakService {
  options: any = {};
  activeNodes: LinkedListService;
  sum: any = {
    width: 0,
    stretch: 0,
    shrink: 0
  };
  lineLengths: any[];
  breaks: any[];
  tmp: any = {
    data: {
      demerits: Infinity
    }
  };
  nodes: any[];
  infinity: number = 10000;

  constructor() {}

  linebreak(nodes: any, lines: any, settings: any) {
    this.nodes = nodes;
    this.options = {
			demerits: {
				line: settings && settings.demerits && settings.demerits.line || 10,
				flagged: settings && settings.demerits && settings.demerits.flagged || 100,
				fitness: settings && settings.demerits && settings.demerits.fitness || 3000
			},
			tolerance: settings && settings.tolerance || 2
		};
    
    this.activeNodes = new LinkedListService();

		this.sum = {
			width: 0,
			stretch: 0,
			shrink: 0
		},
		this.lineLengths = lines,
		this.breaks = [],
		this.tmp = {
			data: {
				demerits: Infinity
			}
    };
    
    let node = this.activeNodes.createNode(this.breakpoint(0, 0, 0, 0, 0, undefined, null));
    this.activeNodes.push(node);

    nodes.forEach((node, index, nodes) => {
			if (node.type === 'box') {
				this.sum.width += node.width;
			} else if (node.type === 'glue') {
				if (index > 0 && nodes[index - 1].type === 'box') {
					this.mainLoop(node, index, nodes);
				}
				this.sum.width += node.width;
				this.sum.stretch += node.stretch;
				this.sum.shrink += node.shrink;
			} else if (node.type === 'penalty' && node.penalty !== this.infinity) {
				this.mainLoop(node, index, nodes);
			}
		});


		if (this.activeNodes.size() !== 0) {
			// Find the best active node (the one with the least total demerits.)
			this.activeNodes.forEach((node) => {
				if (node.data.demerits < this.tmp.data.demerits) {
					this.tmp = node;
				}
			});

			while (this.tmp !== null) {
				this.breaks.push({
					position: this.tmp.data.position,
					ratio: this.tmp.data.ratio
				});
				this.tmp = this.tmp.data.previous;
			}
			return this.breaks.reverse();
		}
		return [];
  }

  breakpoint(position: any, demerits: any, ratio: any, line: any, fitnessClass: any, totals: any, previous: any) {
    return {
      position: position,
      demerits: demerits,
      ratio: ratio,
      line: line,
      fitnessClass: fitnessClass,
      totals: totals || {
        width: 0,
        stretch: 0,
        shrink: 0
      },
      previous: previous
    };
  }

  computeCost(start: any, end: any, active: any, currentLine: any) {
    let width = this.sum.width - active.totals.width,
    stretch = 0,
    shrink = 0,
    // If the current line index is within the list of linelengths, use it, otherwise use
    // the last line length of the list.
    lineLength = currentLine < this.lineLengths.length ? this.lineLengths[currentLine - 1] : this.lineLengths[this.lineLengths.length - 1];

    if (this.nodes[end].type === 'penalty') {
      width += this.nodes[end].width;
    }

    if (width < lineLength) {
      // Calculate the stretch ratio
      stretch = this.sum.stretch - active.totals.stretch;

      if (stretch > 0) {
        return (lineLength - width) / stretch;
      } else {
        return this.infinity;
      }

    } else if (width > lineLength) {
      // Calculate the shrink ratio
      shrink = this.sum.shrink - active.totals.shrink;

      if (shrink > 0) {
        return (lineLength - width) / shrink;
      } else {
        return this.infinity;
      }
    } else {
      // perfect match
      return 0;
    }
  }

  computeSum(breakPointIndex) {
    var result = {
        width: this.sum.width,
        stretch: this.sum.stretch,
        shrink: this.sum.shrink
      },
      i = 0;

    for (i = breakPointIndex; i < this.nodes.length; i += 1) {
      if (this.nodes[i].type === 'glue') {
        result.width += this.nodes[i].width;
        result.stretch += this.nodes[i].stretch;
        result.shrink += this.nodes[i].shrink;
      } else if (this.nodes[i].type === 'box' || (this.nodes[i].type === 'penalty' && this.nodes[i].penalty === -this.infinity && i > breakPointIndex)) {
        break;
      }
    }
    return result;
  }

  // The main loop of the algorithm
  mainLoop(node: any, index: any, nodes: any) {
    var active = this.activeNodes.first(),
      next = null,
      ratio = 0,
      demerits = 0,
      candidates = [],
      badness,
      currentLine = 0,
      tmpSum,
      currentClass = 0,
      fitnessClass,
      candidate,
      newNode;

    // The inner loop iterates through all the active nodes with line < currentLine and then
    // breaks out to insert the new active node candidates before looking at the next active
    // nodes for the next lines. The result of this is that the active node list is always
    // sorted by line number.
    while (active !== null) {

      candidates = [{
        demerits: Infinity
      }, {
        demerits: Infinity
      }, {
        demerits: Infinity
      }, {
        demerits: Infinity
      }];

      // Iterate through the linked list of active nodes to find new potential active nodes
      // and deactivate current active nodes.
      while (active !== null) {
        next = active.next;
        currentLine = active.data.line + 1;
        ratio = this.computeCost(active.data.position, index, active.data, currentLine);

        // Deactive nodes when the distance between the current active node and the
        // current node becomes too large (i.e. it exceeds the stretch limit and the stretch
        // ratio becomes negative) or when the current node is a forced break (i.e. the end
        // of the paragraph when we want to remove all active nodes, but possibly have a final
        // candidate active node---if the paragraph can be set using the given tolerance value.)
        if (ratio < -1 || (node.type === 'penalty' && node.penalty === -this.infinity)) {
          this.activeNodes.remove(active);
        }

        // If the ratio is within the valid range of -1 <= ratio <= tolerance calculate the
        // total demerits and record a candidate active node.
        if (-1 <= ratio && ratio <= this.options.tolerance) {
          badness = 100 * Math.pow(Math.abs(ratio), 3);

          // Positive penalty
          if (node.type === 'penalty' && node.penalty >= 0) {
            demerits = Math.pow(this.options.demerits.line + badness, 2) + Math.pow(node.penalty, 2);
          // Negative penalty but not a forced break
          } else if (node.type === 'penalty' && node.penalty !== -this.infinity) {
            demerits = Math.pow(this.options.demerits.line + badness, 2) - Math.pow(node.penalty, 2);
          // All other cases
          } else {
            demerits = Math.pow(this.options.demerits.line + badness, 2);
          }

          if (node.type === 'penalty' && nodes[active.data.position].type === 'penalty') {
            demerits += this.options.demerits.flagged * node.flagged * nodes[active.data.position].flagged;
          }

          // Calculate the fitness class for this candidate active node.
          if (ratio < -0.5) {
            currentClass = 0;
          } else if (ratio <= 0.5) {
            currentClass = 1;
          } else if (ratio <= 1) {
            currentClass = 2;
          } else {
            currentClass = 3;
          }

          // Add a fitness penalty to the demerits if the fitness classes of two adjacent lines
          // differ too much.
          if (Math.abs(currentClass - active.data.fitnessClass) > 1) {
            demerits += this.options.demerits.fitness;
          }

          // Add the total demerits of the active node to get the total demerits of this candidate node.
          demerits += active.data.demerits;

          // Only store the best candidate for each fitness class
          if (demerits < candidates[currentClass].demerits) {
            candidates[currentClass] = {
              active: active,
              demerits: demerits,
              ratio: ratio
            };
          }
        }

        active = next;

        // Stop iterating through active nodes to insert new candidate active nodes in the active list
        // before moving on to the active nodes for the next line.
        // TODO: The Knuth and Plass paper suggests a conditional for currentLine < j0. This means paragraphs
        // with identical line lengths will not be sorted by line number. Find out if that is a desirable outcome.
        // For now I left this out, as it only adds minimal overhead to the algorithm and keeping the active node
        // list sorted has a higher priority.
        if (active !== null && active.data.line >= currentLine) {
          break;
        }
      }

      tmpSum = this.computeSum(index);

      for (fitnessClass = 0; fitnessClass < candidates.length; fitnessClass += 1) {
        candidate = candidates[fitnessClass];

        if (candidate.demerits < Infinity) {
            newNode = this.activeNodes.createNode(this.breakpoint(index, candidate.demerits, candidate.ratio, candidate.active.data.line + 1, fitnessClass, tmpSum, candidate.active))
          if (active !== null) {
            this.activeNodes.insertBefore(active, newNode);
          } else {
            this.activeNodes.push(newNode);
          }
        }
      }
    }
  }

  glue(width: number, stretch: number, shrink: number) {
		return {
			type: 'glue',
			width: width,
			stretch: stretch,
			shrink: shrink
		};
	};

	box(width: number, value: number) {
		return {
			type: 'box',
			width: width,
			value: value
		};
	};

	penalty(width: number, penalty: number, flagged: number) {
		return {
			type: 'penalty',
			width: width,
			penalty: penalty,
			flagged: flagged
		};
	};
}
